import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the amount to convert (UAH):");
        int amount = Integer.parseInt(in.nextLine());

//        Bank bank = Generate.generate();
//        System.out.println(bank.converterInBank(amount));
//
//        Exchanger exchanger = Generate.generateExchanger();
//        System.out.println(exchanger.converterInExchanger(amount));
//
//        BlackMarket blackMarket = Generate.generateBlackMarket();
//        System.out.println(blackMarket.converterInBlackMarket(amount));


        Storage storages[] = Generate.generate();

//        for (Storage storage : storages) {
//            System.out.println(storage.converter(amount));
//        }

//        for (Storage storage : storages) {
//            if (storage instanceof Exchanger){
//                System.out.println(storage.converter(amount));
//            }
//        }
        Storage bestStorage = storages[0];
        for (int i = 1; i < storages.length; i++) {
            if(storages[i].dollarRate < bestStorage.dollarRate){
                bestStorage = storages[i];
            }
        }
        System.out.println(bestStorage.converter(amount));

    }
}
