public class Bank extends Storage {
    int limitInUAH;

    public Bank(String name, float dollarRate, int limitInUAH) {
        super(name, dollarRate);
        this.limitInUAH = limitInUAH;
    }


//    public String converter(int amount) {
//        if (amount < limitInUAH) {
//            return String.format("%s: %s UAH = %s USD", name, amount, exchange(amount));
//        } else return String.format("Operation is not permitted");
//    }

    public boolean limit(int amount) {
        return amount < limitInUAH;
    }

    @Override
    public String converter(int amount) {
        if (limit(amount)) {
            return super.converter(amount);
        } else return String.format("Operation is not permitted");
    }
}
