public class Generate {
//    public static Bank generate(){
//        return new Bank("PrivatBank", 26.38f, 150000);
//    }
//    public static Exchanger generateExchanger(){
//        return new Exchanger("Foreign Currency Exchange", 26.89f, 20000);
//    }
//    public static BlackMarket generateBlackMarket(){
//        return new BlackMarket("Black Market", 27.21f);
//    }

    public static Storage[] generate() {
        Storage storages[] = new Storage[4];
        storages[0] = new Bank("PrivatBank", 26.38f, 150_000);
        storages[1] = new Bank("Oshchad", 27.38f, 10_000);
        storages[2] = new Exchanger("Foreign Currency Exchange", 26.12f, 20_000);
        storages[3] = new BlackMarket("Black Market", 28.24f);
        return storages;
    }
}

