public class Exchanger extends Storage {
    int limitInUSD;

    public Exchanger(String name, float dollarRate, int limitInUSD) {
        super(name, dollarRate);
        this.limitInUSD = limitInUSD;
    }

//    public String converter(int amount) {
//        if (amount < (limitInUSD * dollarRate)) {
//            return String.format("%s: %s UAH = %s USD", name, amount, exchange(amount));
//        } else return String.format("Operation is not permitted");
//
//    }

    public boolean limit(int amount) {
        return amount < (limitInUSD * dollarRate);
    }

    @Override
    public String converter(int amount) {
        if (limit(amount)) {
            return super.converter(amount);
        } else return String.format("Operation is not permitted");
    }
}
