public class Storage {
    String name;
    float dollarRate;

    public Storage(String name, float dollarRate) {
        this.name = name;
        this.dollarRate = dollarRate;
    }

    public float exchange(int amount) {
        return amount / dollarRate;
    }

    public String converter(int amount) {
        return String.format("%s: %s UAH = %s USD", name, amount, exchange(amount));
    }
}
